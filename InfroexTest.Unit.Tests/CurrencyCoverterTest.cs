using InfroexTest.Services;
using System;
using System.Threading.Tasks;
using Xunit;
using Moq;
using InfroexTest.Repository;
using InfroexTest.Abstract;

namespace InfroexTest.Unit.Tests
{
    public class CurrencyCoverterTest
    {
        private readonly IConvertRepository _repository;
        public CurrencyCoverterTest()
        {
            var mock =  new Mock<IConvertRepository>();
            mock.Setup(r => r.ChangeUserBalanceFromAtoB(
                It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()))
                .Returns(Task.CompletedTask);
            _repository = mock.Object;
        }
        [Theory]
        [InlineData(200, 40, 1.15, 218.5)]
        [InlineData(195.345, 38.976, 0.5667, 105.166910925)]
        public async Task CurrencyConverter_Should_ReturnCorrectData(decimal a, decimal b, decimal rate, decimal expected)
        {
            var converter = new CurrencyConverter(_repository);
            var result = await converter.ConvertAtoB(a, b, rate);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(100, 40, 1, "Incorrect parameter A is 100")]
        [InlineData(160, 4, 1, "Incorrect parameter B is 4")]
        [InlineData(0, 40, 1, "Incorrect parameter A is 0")]
        [InlineData(160, 0, 1, "Incorrect parameter B is 0")]
        [InlineData(-1, 40, 1, "Incorrect parameter A is -1")]
        [InlineData(160, -1, 1, "Incorrect parameter B is -1")]
        [InlineData(1501, 40, 1, "Incorrect parameter A is 1501")]
        [InlineData(160, 51, 1, "Incorrect parameter B is 51")]
        public async Task CurrencyConverter_Should_ReturnArgumentException(decimal a, decimal b, decimal rate, string errorMessage)
        {
            var converter = new CurrencyConverter(_repository);
            try
            {
                var result = await converter.ConvertAtoB(a, b, rate);
            }
            catch(ArgumentException ex)
            {
                Assert.Equal(errorMessage, ex.Message);
            }
        }

        [Fact]
        public async Task CurrencyConverter_Should_CatchRepositoryException()
        {
            Mock<IConvertRepository> mockRepo = new Mock<IConvertRepository>();

            mockRepo.Setup(
                r => r.ChangeUserBalanceFromAtoB(
                    It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()))
                .Throws(new Exception("repository exception"));
            var converter = new CurrencyConverter(mockRepo.Object);
            try
            {
                var result = await converter.ConvertAtoB(160, 40, 1);
            }
            catch (Exception ex)
            {
                Assert.Equal("repository exception", ex.Message);
            }
        }
    }
}
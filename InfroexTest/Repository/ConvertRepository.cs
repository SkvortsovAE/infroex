﻿using InfroexTest.Abstract;

namespace InfroexTest.Repository
{
    public class ConvertRepository : IConvertRepository
    {
        // Здесь зависимости для работы с хранилищем данных
        public async Task ChangeUserBalanceFromAtoB(decimal writeOffAmount, decimal writeUpAmount, decimal taxAmount)
        {
            /* далее логика списания и начисления а так же списания комиссии
             * должна выполняться в рамках ОДНОЙ транзакции
            */
            Task.CompletedTask.Wait();
        }
    }
}

using InfroexTest.Abstract;
using InfroexTest.Repository;
using InfroexTest.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<ICurrencyConverter, CurrencyConverter>();
builder.Services.AddSingleton<IConvertRepository, ConvertRepository>();

// Add services to the container.

var app = builder.Build();

// Configure the HTTP request pipeline.

app.Run();
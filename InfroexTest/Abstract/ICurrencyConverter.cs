﻿namespace InfroexTest.Abstract
{
    public interface ICurrencyConverter
    {
        Task<decimal> ConvertAtoB(decimal a, decimal b, decimal rate);
    }
}

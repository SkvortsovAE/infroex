﻿namespace InfroexTest.Abstract
{
    public interface IConvertRepository
    {
        Task ChangeUserBalanceFromAtoB(decimal writeOffAmount, decimal writeUpAmount, decimal taxAmount);
    }
}

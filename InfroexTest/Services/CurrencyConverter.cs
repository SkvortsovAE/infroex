﻿using InfroexTest.Abstract;

namespace InfroexTest.Services
{
    public class CurrencyConverter : ICurrencyConverter
    {
        //Можно пробросить через конфиг и DI, можно получать из БД
        private const decimal minimalA = 150M;
        private const decimal maximalA = 1500M;
        private const decimal minimalB = 5M;
        private const decimal maximalB = 50M;
        private const decimal tax = 0.05M;

        private readonly IConvertRepository _repository;

        public CurrencyConverter(IConvertRepository repository)
        {
            _repository = repository;
        }

        public async Task<decimal> ConvertAtoB(decimal a, decimal b, decimal rate)
        {
            if(a <= 0 || a < minimalA || a > maximalA)
            {
                throw new ArgumentException($"Incorrect parameter A is {a}");
            }
            if(b <= 0 || b < minimalB || b > maximalB)
            {
                throw new ArgumentException($"Incorrect parameter B is {b}");
            }
            decimal taxAmount = a * tax;
            decimal writeOffAmount = a - taxAmount;
            decimal writeUpAmount = writeOffAmount * rate;
            try
            {
                await _repository.ChangeUserBalanceFromAtoB(writeOffAmount, writeUpAmount, taxAmount);
            }
            catch (Exception)
            {
                //если мы не против вернуть весь callStack
                throw;
            }

            return writeUpAmount;
        }
    }
}
